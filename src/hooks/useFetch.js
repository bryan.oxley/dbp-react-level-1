import { useState, useEffect } from "react";

const useFetch = (url) => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    fetch(url)
      .then((res) => res.json())
      .then((data) => {
        console.log({data})
        setData(data);
        setLoading(false);
      })
      .catch((err) => console.log(err));
  }, [url]);

  return [data, loading];
};

export default useFetch;