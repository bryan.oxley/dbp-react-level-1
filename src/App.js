
import Todolist from "./components/Todolist";
import Home from "./components/Home";
import { Route, Switch } from 'react-router-dom';
import './App.css';
import { TodoLists } from "./components/TodoLists";
import { Details } from "./components/Details";

function App() {
  return (
    <div className="App">
      <div>
        <Switch>
          <Route path='/details'>
            <Details />
          </Route>
          <Route path='/todolists'>
            <TodoLists />
          </Route>
          <Route path='/todo'>
            <Todolist />
          </Route>
          <Route path='/'>
            <Home />
          </Route>
        </Switch>
      </div>
    </div>
  );
}

export default App;
