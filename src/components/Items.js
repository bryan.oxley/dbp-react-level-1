import { Link } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import './Items.css';

/**
 * @typedef {import('../context/store').TodoList} TodoList
 * 
 * @param {{
 *  item?: string;
 *  children?: import('react').ReactNode;
 *  items?: TodoList;
 *  showDetails?: boolean;
 * }} props 
 * @returns 
 */
function Items({ item, children, items, showDetails = false }) {
  const { push } = useHistory();

  return item && children ? (
    <div className='item flex'>
      <p>{item}</p>
      {children ? children : undefined}
    </div>
  ) : (items && items.todoList.length > 0) && (
    <div className='item'>
      {
        items.todoList.map(i => <p>{i.title}</p>)
      }
      {
        showDetails ? (
          <Link style={{ float: 'right' }} to="/details" onClick={() => {
            push('/details', { item: items })
          }}>See Details</Link>
        ) : undefined
      }
    </div>
  );

}

export default Items;