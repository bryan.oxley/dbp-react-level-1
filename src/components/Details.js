import React from 'react';
import { Link } from 'react-router-dom';

import './Todolist.css';
import useFetch from '../hooks/useFetch';
import Items from './Items';
import { useSelector } from 'react-redux';

export const Details = () => {
  const [data, loading] = useFetch('https://www.therogerlab.com/examples/lists-stats.php');
  const selectedTodo = useSelector((state) => state.todos[state.selectedTodoList || 0]);

  return (
    <div className='todolist'>
      <Link to="/todolists">Go Back</Link>
        {
          loading ? <p>Loading Information..</p> : (
            <div className='detailInfo'>
              <p>TOTAL: <b>{data.total}</b></p>
              <p>DONE: <b>{data.done}</b></p>
              <p>DELAYED: <b>{data.delayed}</b></p>
            </div>
          )
        }
        <Items items={selectedTodo} />
    </div>
  );
};
