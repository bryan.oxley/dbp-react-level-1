import { useState } from 'react';
import { useDispatch } from 'react-redux';

import './Todolist.css';
import Items from './Items';
import { Link, useHistory } from 'react-router-dom';
import { todoActions } from '../context';

function Todolist() {
  const dispatch = useDispatch();
  const { push } = useHistory();
  const [todoList, setTodoList] = useState([]);
  const [todoTitle, setTodoTitle] = useState('');
  const [error, setError] = useState(false);

  const addItemIntoTodo = (title) => {
    setTodoTitle(_ => '');
    let el = document.getElementById('new_todo_item_title');
    if (el.value === '') {
      setError(true);
      el.classList.add('input-error')
    }
    setTodoList(old => title !== '' ? [...old, { title, done: false }] : old);
  };

  const deleteItemFromTodo = (index) => {
    setTodoList(old => old.filter((_, i) => i !== index));
  }

  const saveTasksIntoTodos = () => {
    if (todoList.length > 0) {
      dispatch(todoActions.addTodoList(todoList));
      push('todolists')
    }
    else {
      console.log('Empty todo!');
    }
  };

  return (
    <div className='todolist'>
      <div>
        <input id='new_todo_item_title' type="text" value={todoTitle} onChange={(e) => {
          if (e.currentTarget.classList.contains('input-error')) {
            setError(false);
            e.currentTarget.classList.remove('input-error');
          }
          setTodoTitle(e.currentTarget.value);
        }} />
        <button className='commandButton' onClick={() => addItemIntoTodo(todoTitle)}>Add</button>
        <button className='commandButton' onClick={() => saveTasksIntoTodos()}>Save</button>
      </div>
      {
        error && (
          <p className='error-text'>Title cannot be empty.</p>
        )
      }
      <Link to="todolists">Go to ToDo Lists</Link>
      {
        todoList.length > 0 && todoList.map((item, index) =>
          <Items key={index} item={item.title}>
            <button className='close' onClick={() => deleteItemFromTodo(index)}>x</button>
          </Items>
        )
      }
    </div>
  )

}

export default Todolist;