import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import Items from './Items';
import './Todolist.css';

export const TodoLists = () => {
  const todos = useSelector((state) => state.todos)

  return (
    <div className='todolist'>
      <Link to="todo">Add new ToDo</Link>
      {
        todos.length > 0
          ? todos.map((todo, index) => <Items key={index} items={todo} showDetails />)
          : <p>Looks like there is no ToDo list added yet.</p>
      }
    </div>
  );
};
