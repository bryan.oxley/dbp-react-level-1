/**
 * 
 * @typedef {{
 *  todos: TodoList[];
 * }} TodoListStore
 *
 * @typedef {{
 *  title: string;
 *  done: boolean;
 * }} TodoItem
 *  
 * @typedef {{
 *  id: number;
 *  todoList: Array<TodoItem>;
 * }} TodoList
 *
 * @typedef {{
 *  type: 'SET_TODO_LIST';
 *  payload: TodoList
 * }} TodoListAction
 */


import { configureStore, createSlice } from "@reduxjs/toolkit";

/**
 * @typedef {TodoListStore}
 */
export const initialTodoStore = {
  selectedTodoList: null,
  todos: [],
};

const todoSlice = createSlice({
  name: 'todo',
  initialState: initialTodoStore,
  reducers: {
    addTodoList: (state, action) => {
      state.todos.push({
        id: state.todos.length,
        todoList: action.payload,
      })
    },
    setSelectedTodoList: (state, action) => {
      state.selectedTodoList = action.payload;
    },
  },
})

export const todoActions = todoSlice.actions;

export const store = configureStore({
  reducer: todoSlice.reducer,
});
